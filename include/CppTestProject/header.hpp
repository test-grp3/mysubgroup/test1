#include "CppTestProject/cpp_test_project_export.h"
#include <iostream>
#include <string>

namespace prj {
    class CPPTESTPROJECT_EXPORT HelloWorld {
    public:
        void say(std::string const& str);
    };
}
