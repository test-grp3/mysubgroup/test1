include(CMakeFindDependencyMacro)
##################   Dependency   #################################
find_dependency(MCSLogger)
find_dependency(MCSCaptureDevice)
##################   Dependency   #################################
include("${CMAKE_CURRENT_LIST_DIR}/MCSFolderCaptureDeviceTargets.cmake")